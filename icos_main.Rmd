---
title: "ICO returns prediction project"
output:
  html_document:
    df_print: paged
  html_notebook: default
  pdf_document: default
  word_document: default
---
#Download data
```{r}
set.seed(137)
#Download file to the table
f <- read.csv2(file = "icos_main.csv", header = TRUE, dec = ".", sep = ",", encoding = 'UNICOD')
kora <- read.csv2(file = "kora_main.csv", header = TRUE, dec =".", sep = ",", encoding = 'UNICOD')

#icos_main.csv contains data on ~1000 already finished fundraising processes.
#kora_main.csv contains data on the target ongoing ICO fundraising process

#Connect library
library (dplyr)
library (tidyr)
library (base)
library(ggplot2)
#head(f)
```

##Replacing columns. Raised_funds -> #1 column
```{r}
f <- f[,c(names(f)[match("raised_funds",names(f))],names(f)[-match("raised_funds",names(f))])]
#print(names(f))
```

##Delete n/a
```{r}
f[!is.na(f$raised_funds),] -> f_na
library (psych)
describe(f_na)
```

##Remove the ejections (outside the +/- 1.5 sigma)
```{r}
f_ej <- f_na[(f_na$raised_funds < mean(f_na$raised_funds)+sd(f_na$raised_funds)*1.5)&(f_na$raised_funds > mean(f_na$raised_funds)-sd(f_na$raised_funds)*1.5), ] 
#Write prepared data to the file
#write.csv2(f_ej, file = "C:/Users/kat/Desktop/Singularex/china/out_ej.csv")
describe(f_ej)
```



#Cross-correlation matrix
```{r}
library(psych)
pairs.panels(f_ej[,-c(7,10)], pch = ".",lm=TRUE,
             cex.cor=3,
             method = "pearson", # correlation method
             hist.col = "#00AFBB",
             density = TRUE,  # show density plots
             ellipses = TRUE # show correlation ellipses
             )
```

##Fill missing data
```{r}
z = f_ej

z <- tidyr::replace_na(z, replace=list(medium_followers=round(mean(z$medium_followers,na.rm = TRUE))), medium_followers) 

z <- tidyr::replace_na(z, replace=list(medium_subscribers=round(mean(z$medium_subscribers,na.rm = TRUE))), medium_subscribers) 

z <- tidyr::replace_na(z, replace=list(reddit_posts=round(mean(z$reddit_posts,na.rm = TRUE))), reddit_posts)

z <- tidyr::replace_na(z, replace=list(community_count=round(mean(z$community_count,na.rm = TRUE))), community_count)

z <- tidyr::replace_na(z, replace=list(tokens_for_sale=round(mean(z$tokens_for_sale,na.rm = TRUE))), tokens_for_sale)

z <- tidyr::replace_na(z, replace=list(tokens_distributed=round(mean(z$tokens_distributed,na.rm = TRUE))), tokens_distributed)

z <- tidyr::replace_na(z, replace=list(github_stars=round(mean(z$github_stars,na.rm = TRUE))), github_stars)

z <- tidyr::replace_na(z, replace=list(github_code_amount=round(mean(z$github_code_amount,na.rm = TRUE))), github_code_amount)

z <- tidyr::replace_na(z, replace=list(github_code_amount=round(mean(z$github_code_amount,na.rm = TRUE))), github_code_amount)

z <- tidyr::replace_na(z, replace=list(bazaar_total_rate=round(mean(z$bazaar_total_rate,na.rm = TRUE))), bazaar_total_rate)

z <- tidyr::replace_na(z, replace=list(ico_holder_total_rate=round(mean(z$ico_holder_total_rate,na.rm = TRUE))), ico_holder_total_rate)

z <- tidyr::replace_na(z, replace=list(alexa_rank=round(mean(z$alexa_rank,na.rm = TRUE))), alexa_rank)

z <- tidyr::replace_na(z, replace=list(alexa_rank=round(mean(z$alexa_rank,na.rm = TRUE))), alexa_rank)

z <- tidyr::replace_na(z, replace=list(telegram_members=round(mean(z$telegram_members,na.rm = TRUE))), telegram_members)

z <- tidyr::replace_na(z, replace=list(twitter_followers=round(mean(z$twitter_followers,na.rm = TRUE))), twitter_followers)

z <- tidyr::replace_na(z, replace=list(twitter_tweets=round(mean(z$twitter_tweets,na.rm = TRUE))), twitter_tweets)

z <- tidyr::replace_na(z, replace=list(bitcointalk_activity=round(mean(z$bitcointalk_activity,na.rm = TRUE))), bitcointalk_activity)

z <- tidyr::replace_na(z, replace=list(bitcointalk_views=round(mean(z$bitcointalk_views,na.rm = TRUE))), bitcointalk_views)

z <- tidyr::replace_na(z, replace=list(fb_likes=round(mean(z$fb_likes,na.rm = TRUE))), fb_likes)

z <- tidyr::replace_na(z, replace=list(fb_posts=round(mean(z$fb_posts,na.rm = TRUE))), fb_posts)

z <- tidyr::replace_na(z, replace=list(reddit_subscribers=round(mean(z$reddit_subscribers,na.rm = TRUE))), reddit_subscribers)
```

##Scale the Raised_funds var
```{r}
m_rf<-mean(z$raised_funds)
sd_rf<-sd(z$raised_funds)
f_sc <- mutate(z,raised_funds=(raised_funds-m_rf)/sd_rf)
```

##Create TRAIN & TEST samples
```{r}
f_ej_na <- tidyr::drop_na(z)
#creating the index for ej
idx <- caret::createDataPartition(f_ej_na$raised_funds,     
                                  times = 1,      
                                  p = 0.8,     
                                  list = FALSE) 
f_ej_train <- f_ej_na[idx,] 
f_ej_test <- f_ej_na[-idx,] 

f_sc_na <- tidyr::drop_na(f_sc)
#creating the index for sc
idx <- caret::createDataPartition(f_sc_na$raised_funds,     
                                  times = 1,      
                                  p = 0.8,     
                                  list = FALSE) 
f_sc_train <- f_sc_na[idx,] 
f_sc_test <- f_sc_na[-idx,] 
```

#Linear regression - ej
```{r}
lr_1 <- lm(data = f_ej_train, raised_funds ~ alexa_rank + telegram_members + twitter_followers + bitcointalk_activity + fb_likes + medium_followers + reddit_subscribers + reddit_posts + github_stars + ico_holder_total_rate)
summary(lr_1)
```
```{r}
lr_2 <- lm(data = f_ej_train, raised_funds ~ alexa_rank + telegram_members + medium_followers + reddit_subscribers + github_stars)
summary(lr_2)
```

##MSE LR - ej
```{r}
p1 <- predict(lr_1, f_ej_test)
mse_lr1<-sum((f_ej_test$raised_funds-p1)^2)/length(p1)
p2 <- predict(lr_2, f_ej_test)
mse_lr2<-sum((f_ej_test$raised_funds-p2)^2)/length(p2)
cat(c(mse_lr1,mse_lr2),"\n")

k1 <- predict(lr_1, kora)
k2 <- predict(lr_2, kora)
cat(c(k1,k2))
```

#Linear regression - sc
```{r}
lr_1 <- lm(data = f_sc_train,  raised_funds ~ alexa_rank + telegram_members + twitter_followers + bitcointalk_activity + fb_likes + medium_followers + reddit_subscribers + reddit_posts + github_stars + ico_holder_total_rate)
summary(lr_1)
```
```{r}
lr_2 <- lm(data = f_sc_train, raised_funds ~ alexa_rank + telegram_members + medium_followers + reddit_subscribers + github_stars)
summary(lr_2)
```

##MSE LR sc
```{r}
p1 <- predict(lr_1, f_sc_test)
mse_lr1<-sum((f_sc_test$raised_funds-p1)^2)/length(p1)
p2 <- predict(lr_2, f_sc_test)
mse_lr2<-sum((f_sc_test$raised_funds-p2)^2)/length(p2)
cat(c(mse_lr1,mse_lr2),"\n")

k3 <- predict(lr_1, kora)*sd_rf + m_rf
k4 <- predict(lr_2, kora)*sd_rf + m_rf
cat(c(k3,k4))
# 
ggplot(data=f_sc_test) +
  geom_point(aes(reddit_subscribers, raised_funds),color = 4) +
  geom_point(aes(reddit_subscribers, p2),color = 6)
```

#NN 
```{r results='hide'}
library(nnet)
nn_1 <- nnet(data = f_sc_train, raised_funds ~ alexa_rank + telegram_members + twitter_followers + twitter_tweets + bitcointalk_activity + bitcointalk_views + fb_likes + fb_posts + medium_followers + reddit_subscribers + reddit_posts + community_count + ico_bench_total_rate + tokens_for_sale + tokens_distributed + github_stars + github_code_amount + bazaar_total_rate + ico_holder_total_rate, linout = TRUE ,size = 5, maxit = 10000)

nn_2 <- nnet(data = f_sc_train, raised_funds ~ alexa_rank + telegram_members + medium_followers + reddit_subscribers + github_stars, linout = TRUE ,size = 3, maxit = 10000)
```

##MSE NN
```{r}
p1 <- predict(nn_1, f_sc_test)
mse_nn1<-sum((f_sc_test$raised_funds-p1)^2)/length(p1)
p2 <- predict(nn_2, f_sc_test)
mse_nn2<-sum((f_sc_test$raised_funds-p2)^2)/length(p2)
cat(c(mse_nn1,mse_nn2),"\n")

k5 <- predict(nn_1, kora)*sd_rf + m_rf
k6 <- predict(nn_2, kora)*sd_rf + m_rf
cat(c(k5,k6))
# 
# ggplot(data=f_sc_test) + 
#   geom_point(aes(telegram_members, raised_funds),color = 4) +
#   geom_point(aes(telegram_members, p2),color = 6)
```

#Tree
```{r}
library(rpart)
tree <- rpart(data = f_ej_train, raised_funds ~ alexa_rank + telegram_members + medium_followers + reddit_subscribers + github_stars)
library(rpart.plot)
prp(tree)
```

##MSE Tree
```{r}
p <- predict(tree, f_ej_test)
mse_tree<-sum((f_ej_test$raised_funds-p)^2)/length(p)
mse_tree

k7 <- predict(tree, kora)
k7
# 
# ggplot(data=f_ej_test) + 
#   geom_point(aes(github_stars, raised_funds),color = 4) +
#   geom_point(aes(github_stars, p),color = 6)

```

#Hierarchical clustering
```{r}
f_ej_na <- tidyr::drop_na(f_ej)
library(stats)
library(cluster)
options(scipen=5)
d <- dist(z[,c('alexa_rank', 'telegram_members', 'medium_followers', 'reddit_subscribers', 'github_stars')])
res.hc <- hclust(d, method = "ward.D" )
grp <- cutree(res.hc, k = 6)  # Разрезание дерева на группы

col = c("red","pink","purple","green","orange","yellow","blue","navy")
plot(res.hc, cex = 0.7, labels = FALSE, las=2)
rect.hclust(res.hc, k = 6, border = col)
ff<-cbind(z,grp)
```

##Number:
```{r}
count(ff,grp) %>% print()
```

##Average:
```{r}
av <- group_by(ff,grp) %>% 
  summarise(.,
            avg_funds=round(mean(raised_funds),0),
            avg_alexa=round(mean(alexa_rank),2),
            avg_medium=round(mean(medium_followers),2),
            avg_reddit=round(mean(reddit_subscribers),2),
            avg_github=round(mean(github_stars),2)) %>%
  print()
```

##Standart Deviation:
```{r}
group_by(ff,grp) %>% 
  summarise(.,
            sd_funds=round(sd(raised_funds),0),
            sd_alexa=round(sd(alexa_rank),2),
            sd_medium=round(sd(medium_followers),2),
            sd_reddit=round(sd(reddit_subscribers),2),
            sd_github=round(sd(github_stars),2)) %>%
  print()
```

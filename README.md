**Run the project**
To run the project, download the repository and open *icos_main.Rmd* in the R Studio.
*icos_main.csv* and *kora_main.csv* source files should be in the same directory. Otherwise, the path to these files should be specified. 

**See the results**
Results of modeling are in the *results.html* file

**Project description**
This is a modeling part of a project "Factors that determine the success of ICO fundraising"

The following parameters were used as model inputs:

• Facebook: number of posts, followers, likes

• Twitter: number of tweets, retweets, followers

• Medium: number of followers

• Github: number of code lines, watchers, stars

• Reddit: number of posts, comments, subscribers

• Bitcointalk: number of posts, views, merit

• ICObench, ICOholder, ICObazaar: ratings

• Telegram: number of channel members

• Team: team experience & success score

• Website: Alexa rank

The following models were applied: 

• Linear regression

• Neural networks

• Hierarchical clustering

• Decision trees